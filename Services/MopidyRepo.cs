using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace test_api.Services {
    public class MopidyRepo : IMopidy {
        private async Task<object> _apiRequest (dynamic mopidyRequestModel) {
            using (var client = new HttpClient()) {
            dynamic data = null;
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(mopidyRequestModel);
            client.BaseAddress = new System.Uri (MopidyConnection.BaseAddress);
            client.DefaultRequestHeaders.Accept
                .Add (new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequestMessage request =
                    new HttpRequestMessage (HttpMethod.Post, MopidyConnection.RequestUri);

                request.Content = new StringContent (json, Encoding.UTF8, "application/json");

                await client.SendAsync(request)
                    .ContinueWith(async responseTask => {
                        if (responseTask.Result.IsSuccessStatusCode) {
                            using (HttpContent content = responseTask.Result.Content) {
                                data = JsonConvert.DeserializeObject(await content.ReadAsStringAsync());
                            }   
                        }
                    });

            if (data == null)
                return null;
            return data;
            }
        }


        public Task<object> AddToTrackList (string uri) {
            throw new System.NotImplementedException ();
        }

        public Task<object> GetPlaylists () {
            dynamic requestModel = new ExpandoObject ();
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.playlists.as_list";
            requestModel.@params = new List<object> ();
            return _apiRequest(requestModel);
        }

        public Task<object> Play(int id)
        {
            dynamic requestModel = new ExpandoObject();
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.playback.play";
            requestModel.@params = new List<object>(){null, id};
            return _apiRequest(requestModel);
        }

        public Task<object> Play()
        {
            dynamic requestModel = new ExpandoObject();
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.playback.play";
            requestModel.@params = new List<object> (){null, null};
            return _apiRequest(requestModel);
        }

        public Task<object> Pause()
        {
            dynamic requestModel = new ExpandoObject();
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.playback.pause";
            requestModel.@params = new List<object> ();
            return _apiRequest(requestModel);
        }

        public Task<object> Resume()
        {
            dynamic requestModel = new ExpandoObject();
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.playback.resume";
            requestModel.@params = new List<object> ();
            return _apiRequest(requestModel);
        }

        public Task<object> Shuffle()
        {
            dynamic requestModel = new ExpandoObject();            
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.tracklist.shuffle";
            requestModel.@params = new List<object> ();
            return _apiRequest(requestModel);
        }

        public Task<object> GetTrackList()
        {
            dynamic requestModel = new ExpandoObject();            
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.tracklist.get_tl_trakcs";
            requestModel.@params = new List<object> ();
            return _apiRequest(requestModel);
        }

        public async Task<object> GetNext()
        {
            dynamic requestModel = new ExpandoObject();            
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.tracklist.next_track";
            requestModel.@params = new List<object>(){null};           
            return await _apiRequest(requestModel);
        }

        public Task<object> GetPrevious()
        {
            dynamic requestModel = new ExpandoObject();            
            requestModel.id = 1;
            requestModel.jsonrpc = "2.0";
            requestModel.method = "core.tracklist.previous_track";
            requestModel.@params = new List<object>(){null};
            return _apiRequest(requestModel);
        }
    }
}