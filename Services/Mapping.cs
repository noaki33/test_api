using test_api.Models;

namespace test_api.Services{
    public class Mapping{
        public Track JsonToTrack(dynamic data){
            var model = new Track();
            model.Id = data.result.tlid;
            model.Title = data.result.track.name;
            model.Uri = data.result.track.uri;
            foreach(var artist in data.result.track.artists){
                model.Artists.Add(new Artist(){
                    Name = artist.name,
                    Uri = artist.uri
                });
            }
            return model;
        }
    }
}