using System.Threading.Tasks;

namespace test_api.Services{
    public interface IMopidy{
        Task<object> Play(int id);
        Task<object> Play();
        Task<object> Pause();
        Task<object> Resume();
        Task<object> Shuffle();
        Task<object> GetNext();
        Task<object> GetPrevious();
        Task<object> GetPlaylists();
        Task<object> GetTrackList();
        Task<object> AddToTrackList(string uri);
    }
}