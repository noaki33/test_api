namespace test_api.Models{
    public class Playlist{
        public string Name { get; set; }
        public System.Uri Uri { get; set; }
        public string Author { get; set; }
    }
}