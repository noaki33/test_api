namespace test_api.Models{
    public class Artist{
        public Artist(){}
        public Artist(string name, string uri){
            Name = name;
            Uri = uri;
        }
        public string Name { get; set; }
        public string Uri { get; set; } 
    }
}