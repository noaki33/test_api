using System.Collections.Generic;

namespace test_api.Models{
    public class Track{
        public Track(){
            Artists = new List<Artist>();
        }
        public string Title { get; set; }
        public List<Artist> Artists { get; set; }
        public string Uri { get; set; }
        public int Id{ get; set; }
    }
}