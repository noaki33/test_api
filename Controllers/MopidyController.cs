using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using test_api.Services;

namespace test_api.Controllers
{
    [Route("api/[controller]")]
    public class MopidyController : Controller
    {
        private readonly IMopidy _mopidy = new MopidyRepo();
        private readonly Mapping _map = new Mapping();

        [HttpGet("playlists")]
        public async Task<JsonResult> Playlists(int id)
        {
            return Json(await _mopidy.GetPlaylists());
        }

        [HttpGet("play/{id}")]
        public async Task<JsonResult> Play(int id)
        {
            return Json(await _mopidy.Play(id));
        }

        [HttpGet("play")]
        public async Task<JsonResult> Play(){
            return Json(await _mopidy.Play());
        }

        [HttpGet("pause")]
        public async Task<JsonResult> Pause()
        {
            return Json(await _mopidy.Pause());
        }

        [HttpGet("next")]
        public async Task<JsonResult> Next(){
            return Json(_map.JsonToTrack(await _mopidy.GetNext()));
        }

        [HttpGet("previous")]
        public async Task<JsonResult> Previous(){
             return Json(_map.JsonToTrack(await _mopidy.GetPrevious()));
        }

    }
}
